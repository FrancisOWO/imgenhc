# 基于直方图均衡化的图像增强

### 图片大小更正

resize.py：更正单张图片的大小

resize_all.py：更正某文件夹下所有图片的大小

### 直方图均衡化

my_convert.py：颜色模型转换

my_tools.py：直方图均衡相关计算

my_plots.py：绘制直方图

demo_*.py：对某张图片做直方图均衡

all_*.py：对某个文件夹下所有图片做直方图均衡

% GRAY：灰度直方图均衡

% BGR：彩色直方图均衡（BGR三通道）

% HSI：彩色直方图均衡（HSI的I通道）

% YUV：彩色直方图均衡（YUV的Y通道）