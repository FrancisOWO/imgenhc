################## 基于直方图均衡化的图像增强 ##################
import cv2 as cv
import my_plots as myplt
import my_tools as mytool
import os

if __name__ == '__main__':
    list_dirs = os.walk('images_resize')
    for path, dirs, files in list_dirs:
        for f in files:
            # 图片路径
            filename = os.path.join(path, f)
            # 原图
            src_img = cv.imread(filename)
            shape = src_img.shape     # 图像大小（高度,宽度,通道数）
            pix_n = shape[0]*shape[1] # 像素数

            ########## 直方图均衡化 ##########
            chan = 3
            level = 256
            hist_seq = [0,0,0]
            cum_seq = [0,0,0]
            norm_cum_seq = [0,0,0]
            sim_seq = [0,0,0]
            eq_hist_seq = [0,0,0]
            enhc_img = src_img.copy()
            for i in range(chan):
                # 计算BGR各分量的直方图序列
                hist_seq[i] = mytool.getHist(src_img, level, i)
                # 计算累积分布序列
                cum_seq[i] = mytool.getCum(hist_seq[i])
                norm_cum_seq[i] = mytool.getNorm(cum_seq[i], pix_n)   # 归一化
                # 计算近似灰度级
                sim_seq[i] = mytool.getSim(norm_cum_seq[i], level)
                # 直方图均衡化
                eq_hist_seq[i] = mytool.histEq(hist_seq[i], sim_seq[i])
                # 图像增强（基于直方图均衡）
                enhc_img = mytool.imgEnhc(enhc_img, sim_seq[i], i)

            ########## 保存图像 ##########
            # 彩图（增强后）
            cv.imwrite('images_output_rgb/' + f, enhc_img)
