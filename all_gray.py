################## 基于直方图均衡化的图像增强 ##################
import cv2 as cv
import my_plots as myplt
import my_tools as mytool
import my_convert as myconv
from my_convert import Conv
import os

if __name__ == '__main__':
    list_dirs = os.walk('images_resize')
    for path, dirs, files in list_dirs:
        for f in files:
            # 图片路径
            filename = os.path.join(path, f)

            # 原图
            src_img = cv.imread(filename)
            shape = src_img.shape     # 图像大小（高度,宽度,通道数）
            pix_n = shape[0]*shape[1] # 像素数
            # 灰度图
            #gray_img = cv.cvtColor(src_img, cv.COLOR_BGR2GRAY)
            gray_img = myconv.img_convert(src_img, Conv.COLOR_BGR2GRAY)
            
            ########## 直方图均衡化 ##########
            # 计算灰度直方图序列
            level = 256
            hist_seq = mytool.getHist(gray_img, level, -1)
            # 计算累积分布序列
            cum_seq = mytool.getCum(hist_seq)
            norm_cum_seq = mytool.getNorm(cum_seq, pix_n)   # 归一化
            # 计算近似灰度级
            sim_seq = mytool.getSim(norm_cum_seq, level)

            # 直方图均衡化
            eq_hist_seq = mytool.histEq(hist_seq, sim_seq)
            # 图像增强（基于直方图均衡）
            enhc_img = mytool.imgEnhc(gray_img, sim_seq, -1)

            ########## 保存图像 ##########
            # 灰度图（增强后）
            cv.imwrite('images_output_gray/' + f, enhc_img)
