################## 基于直方图均衡化的图像增强 ##################
import cv2 as cv
import my_plots as myplt
import my_tools as mytool
import my_convert as myconv
from my_convert import Conv
import os

if __name__ == '__main__':
    list_dirs = os.walk('images_resize')
    for path, dirs, files in list_dirs:
        for f in files:
            # 图片路径
            filename = os.path.join(path, f)
            # 原图
            src_img = cv.imread(filename)
            shape = src_img.shape     # 图像大小（高度,宽度,通道数）
            pix_n = shape[0]*shape[1] # 像素数
            # BGR转YUV
            #YUV_img = cv.cvtColor(src_img, cv.COLOR_BGR2YUV)
            YUV_img = myconv.img_convert(src_img, Conv.COLOR_BGR2YUV)

            ########## 直方图均衡化 ##########
            # 计算Y分量的直方图序列
            level = 256
            chan = 0    # I通道
            hist_seq = mytool.getHist(YUV_img, level, chan)
            # 计算累积分布序列
            cum_seq = mytool.getCum(hist_seq)
            norm_cum_seq = mytool.getNorm(cum_seq, pix_n)   # 归一化
            # 计算近似灰度级
            sim_seq = mytool.getSim(norm_cum_seq, level)

            # 直方图均衡化
            eq_hist_seq = mytool.histEq(hist_seq, sim_seq)
            # 图像增强（基于直方图均衡）
            enhc_img = mytool.imgEnhc(YUV_img, sim_seq, chan)

            # YUV转BGR
            #enhc_img = cv.cvtColor(enhc_img, cv.COLOR_YUV2BGR)
            enhc_img = myconv.img_convert(enhc_img, Conv.COLOR_YUV2BGR)
            
            ########## 保存图像 ##########
            # 彩图（增强后）
            cv.imwrite('images_output_yuv/' + f, enhc_img)
