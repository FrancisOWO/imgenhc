################## 基于直方图均衡化的图像增强 ##################
import cv2 as cv
import my_plots as myplt
import my_tools as mytool

if __name__ == '__main__':
    # 图片路径
    filename = "demo.jpg"
    # 原图
    src_img = cv.imread(filename)
    shape = src_img.shape     # 图像大小（高度,宽度,通道数）
    pix_n = shape[0]*shape[1] # 像素数

    ########## 直方图均衡化 ##########
    chan = 3
    level = 256
    hist_seq = [0,0,0]
    cum_seq = [0,0,0]
    norm_cum_seq = [0,0,0]
    sim_seq = [0,0,0]
    eq_hist_seq = [0,0,0]
    enhc_img = src_img.copy()
    for i in range(chan):
        # 计算BGR各分量的直方图序列
        hist_seq[i] = mytool.getHist(src_img, level, i)
        # 计算累积分布序列
        cum_seq[i] = mytool.getCum(hist_seq[i])
        norm_cum_seq[i] = mytool.getNorm(cum_seq[i], pix_n)   # 归一化
        # 计算近似灰度级
        sim_seq[i] = mytool.getSim(norm_cum_seq[i], level)
        # 直方图均衡化
        eq_hist_seq[i] = mytool.histEq(hist_seq[i], sim_seq[i])
        # 图像增强（基于直方图均衡）
        enhc_img = mytool.imgEnhc(enhc_img, sim_seq[i], i)

    ########## 打印信息 ##########
    #print("shape =",shape,", pix_n =",pix_n)
    #print("灰度直方图序列（原始）：\n", hist_seq)
    #print("累积序列：\n", cum_seq)
    #print("累积序列归一化：\n", norm_cum_seq)
    #print("近似灰度级序列：\n", sim_seq)
    #print("灰度直方图序列（原始）：\n", hist_seq)
    #print("灰度直方图序列（均衡化）：\n", eq_hist_seq)

    ########## 绘制图表 ##########
    # 绘制灰度直方图
    myplt.pltHist(hist_seq[0], "B分量直方图（原始）","灰度级","频次")
    myplt.pltHist(eq_hist_seq[0], "B分量直方图（均衡化）","灰度级","频次")
    myplt.pltHist(hist_seq[1], "G分量直方图（原始）","灰度级","频次")
    myplt.pltHist(eq_hist_seq[1], "G分量直方图（均衡化）","灰度级","频次")
    myplt.pltHist(hist_seq[2], "R分量直方图（原始）","灰度级","频次")
    myplt.pltHist(eq_hist_seq[2], "R分量直方图（均衡化）","灰度级","频次")
    ########## 显示图像 ##########
    # 原图
    win_name = "Source"
    cv.namedWindow(win_name, 0)
    cv.imshow(win_name, src_img)
    # 彩图（增强后）
    win_name = "Color (after enchancement)"
    cv.namedWindow(win_name, 0)
    cv.imshow(win_name, enhc_img)
    # 退出
    cv.waitKey(0)
    cv.destroyAllWindows()
