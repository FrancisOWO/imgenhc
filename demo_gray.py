################## 基于直方图均衡化的图像增强 ##################
import cv2 as cv
import my_plots as myplt
import my_tools as mytool
import my_convert as myconv
from my_convert import Conv

if __name__ == '__main__':
    # 图片路径
    filename = "demo.jpg"
    # 原图
    src_img = cv.imread(filename)
    shape = src_img.shape     # 图像大小（高度,宽度,通道数）
    pix_n = shape[0]*shape[1] # 像素数
    # 灰度图
    #gray_img = cv.cvtColor(src_img, cv.COLOR_BGR2GRAY)
    gray_img = myconv.img_convert(src_img, Conv.COLOR_BGR2GRAY)

    ########## 直方图均衡化 ##########
    # 计算灰度直方图序列
    level = 256
    hist_seq = mytool.getHist(gray_img, level, -1)
    # 计算累积分布序列
    cum_seq = mytool.getCum(hist_seq)
    norm_cum_seq = mytool.getNorm(cum_seq, pix_n)   # 归一化
    # 计算近似灰度级
    sim_seq = mytool.getSim(norm_cum_seq, level)

    # 直方图均衡化
    eq_hist_seq = mytool.histEq(hist_seq, sim_seq)
    # 图像增强（基于直方图均衡）
    enhc_img = mytool.imgEnhc(gray_img, sim_seq, -1)

    ########## 打印信息 ##########
    #print("shape =",shape,", pix_n =",pix_n)
    print("灰度直方图序列（原始）：\n", hist_seq)
    print("累积序列：\n", cum_seq)
    #print("累积序列归一化：\n", norm_cum_seq)
    print("近似灰度级序列：\n", sim_seq)
    print("灰度直方图序列（原始）：\n", hist_seq)
    print("灰度直方图序列（均衡化）：\n", eq_hist_seq)

    ########## 绘制图表 ##########
    # 绘制灰度直方图
    myplt.pltHist(hist_seq, "灰度直方图（原始）","灰度级","频次")
    myplt.pltHist(eq_hist_seq, "灰度直方图（均衡化）","灰度级","频次")

    ########## 显示图像 ##########
    # 原图
    win_name = "Source"
    cv.namedWindow(win_name, 0)
    cv.imshow(win_name, src_img)
    # 灰度图（增强前）
    win_name = "Gray (before enchancement)"
    cv.namedWindow(win_name, 0)
    cv.imshow(win_name, gray_img)
    # 灰度图（增强后）
    win_name = "Gray (after enchancement)"
    cv.namedWindow(win_name, 0)
    cv.imshow(win_name, enhc_img)
    # 退出
    cv.waitKey(0)
    cv.destroyAllWindows()
