import numpy as np

# 颜色模型转换
class Conv:
    COLOR_BGR2GRAY  = 0
    COLOR_BGR2HSI   = 1
    COLOR_HSI2BGR   = 2
    COLOR_BGR2HSV   = 3
    COLOR_HSV2BGR   = 4
    COLOR_BGR2YUV   = 5
    COLOR_YUV2BGR   = 6

# 像素点BGR转GRAY
def pix_BGR2GRAY(BGR_pix):
    # 取BGR分量
    # !!! 三通道顺序是BGR，不是RGB
    B,G,R = BGR_pix
    # gray~[0,255]，+0.5用来四舍五入
    gray = int((299*R + 587*G + 114*B)/1000+0.5)
    if gray > 255:
        gray = 255
    return gray

# 像素点BGR转HSI
def pix_BGR2HSI(BGR_pix):
    # 取BGR分量
    B,G,R = BGR_pix
    # B/G/R~[0,1]
    B,G,R = B/255, G/255, R/255
    # 计算theta
    tn = (R-G) + (R-B)
    td = 2*np.sqrt((R-G)*(R-G)+(R-B)*(G-B))
    theta = 0
    if(td != 0):
        theta = np.arccos(tn/td)
    # 计算HSI
    # H：色调
    H = theta
    if(G < B):
        H = 2*np.pi - theta
    # H: [0,2*pi]->[0,1]
    H = H/(2*np.pi)
    # S：饱和度
    S = 0
    min_rgb = min(min(R,G),B)
    sum_rgb = R + G + B
    if(sum_rgb > 0):
        S = 1 - 3*min_rgb/sum_rgb
    # I：亮度
    I = sum_rgb/3
    # 返回像素点的HSI
    H,S,I = H*255,S*255,I*255
    return [H,S,I]

# 像素点HSI转BGR
def pix_HSI2BGR(HSI_pix):
    # 取HSI分量
    H,S,I = HSI_pix
    # H映射到[0,2*pi]，S,I映射到[0,1]
    H,S,I = 2*np.pi*H/255, S/255, I/255
    # 按H分段：[0,2*pi)中每段2*pi/3
    pi3d = np.pi/3
    H3 = H*3
    R,G,B = 0,0,0    
    if  H3 < 2*np.pi:   # [0,2*pi/3)
        B = I*(1-S)
        R = I*(1+S*np.cos(H)/np.cos(pi3d-H))
        G = 3*I - (R+B)
    elif H3 < 4*np.pi:  # [2*pi/3,4*pi/3)
        H -= 2*pi3d
        R = I*(1-S)
        G = I*(1+S*np.cos(H)/np.cos(pi3d-H))
        B = 3*I - (R+G)
    else:               # [4*pi/3,2*pi)
        H -= 4*pi3d
        G = I*(1-S)
        B = I*(1+S*np.cos(H)/np.cos(pi3d-H))
        R = 3*I - (G+B)        
    # 返回像素点的BGR
    R,G,B = R*255,G*255,B*255
    R = 255 if R > 255 else R
    G = 255 if G > 255 else G
    B = 255 if B > 255 else B
    # !!! 三通道的顺序是BGR
    return [B,G,R]

# 像素点BGR转HSV
def pix_BGR2HSV(BGR_pix):
    # 取BGR分量，归一化
    B,G,R = BGR_pix
    B,G,R = B/255, G/255, R/255
    # 计算HSI
    # V：明度
    V = max(max(R,G),B)
    # S：饱和度
    S = 0
    min_rgb = min(min(R,G),B)
    diff = V - min_rgb
    if V != 0:
        S = diff/V
    # H：色调
    # [0,2*pi)对应[0,1)，pi/3对应1/6
    H = 0
    if R != G or R != B:    # R,G,B不全相等
        if V == R:
            H = (G-B)/6/diff
        elif V == G:
            H = 1/3 + (B-R)/6/diff
        else:   # B == V
            H = 2/3 + (R-G)/6/diff
    if H < 0:
        H = H + 1
    # 返回像素点的HSI
    H,S,V = H*255,S*255,V*255
    return [H,S,V]

# 像素点HSV转BGR
def pix_HSV2BGR(HSV_pix):
    # 取HSI分量
    H,S,V = HSV_pix
    # H/S/V~[0,1]
    H,S,V = H/255, S/255, V/255
    diff = V*S
    X = 0
    if int(6*H)%2 == 1:
        X = diff
    min_rgb = V - diff
    # 按H分段：[0,1)中每段1/6，对应[0,2*pi)中每段pi/3
    H6 = 6*H
    R,G,B = 0,0,0
    if   H6 < 1:    # [0,1/6)
        R,G,B = diff,X,0
    elif H6 < 2:    # [1/6,1/3)
        R,G,B = X,diff,0
    elif H6 < 3:    # [1/3,1/2)
        R,G,B = 0,diff,X
    elif H6 < 4:    # [1/2,2/3)
        R,G,B = 0,X,diff
    elif H6 < 5:    # [2/3,5/6)
        R,G,B = X,0,diff
    else:           # [5/6,1)
        R,G,B = diff,0,X
    # 返回像素点的BGR
    R,G,B = (R+min_rgb)*255,(G+min_rgb)*255,(B+min_rgb)*255
    R = 255 if R > 255 else R
    G = 255 if G > 255 else G
    B = 255 if B > 255 else B
    # !!! 三通道的顺序是BGR
    return [B,G,R]

# 像素点BGR转YUV
def pix_BGR2YUV(BGR_pix):
    # 取BGR分量
    B,G,R = BGR_pix
    # 计算YUV，未量化：Y/U/V~[0,255]
    '''
    Y = int(0.299*R + 0.587*G + 0.114*B)
    U = int(-0.169*R - 0.331*G + 0.500*B) + 128
    V = int(0.500*R - 0.419*G - 0.081*B) + 128
    '''
    Y = (( 77*R + 150*G + 29*B) >> 8)
    U = ((-44*R - 87*G + 131*B) >> 8) + 128
    V = ((131*R - 110*G - 21*B) >> 8) + 128

    '''
    # 量化为TV Range：Y~[16,235]，U/V~[16,240]
    Y = ( 66*R + 129*G + 25*B) >> 8 + 16
    U = (-38*R - 74*G + 112*B) >> 8 + 128
    V = (112*R - 94*G -  18*B) >> 8 + 128
    '''
    Y = 255 if Y > 255 else Y
    U = 255 if U > 255 else U
    V = 255 if V > 255 else V
    Y = 0 if Y < 0 else Y
    U = 0 if U < 0 else U
    V = 0 if V < 0 else V
    return [Y,U,V]

# 像素点YUV转BGR
def pix_YUV2BGR(YUV_pix):
    # 取YUV分量
    Y,U,V = YUV_pix
    # 计算BGR
    '''
    R = Y + 1.403*(V-128)
    G = Y - 0.343*(U-128) - 0.714*(V-128)
    B = Y + 1.770*(U-128)
    '''
    R = Y + ((359*(V-128)) >> 8)
    G = Y - ((88*(U-128) + 183*(V-128)) >> 8)
    B = Y + ((453*(U-128)) >> 8)
    
    '''
    # 量化，Y~[16,235]，U/V~[16,240]
    R = (298*Y + 411*V - 57344) >> 8
    G = (298*Y - 101*U - 211*V + 34739) >> 8
    B = (298*Y + 519*U - 71117) >> 8
    '''
    R,G,B = int(R),int(G),int(B)
    R = 255 if R > 255 else R
    G = 255 if G > 255 else G
    B = 255 if B > 255 else B
    R = 0 if R < 0 else R
    G = 0 if G < 0 else G
    B = 0 if B < 0 else B
    return [B,G,R]

# 图片颜色模型转换
def img_convert(img_in, mode):
    shape = img_in.shape
    height = shape[0]
    width = shape[1]
    if Conv.COLOR_BGR2GRAY == mode:
        img_out = np.empty((height,width), np.uint8)
    else:
        img_out = np.empty(shape, np.uint8)
    for i in range(height):
        for j in range(width):
            if   mode == Conv.COLOR_BGR2GRAY:
                img_out[i][j] = pix_BGR2GRAY(img_in[i][j])
            elif mode == Conv.COLOR_BGR2HSI:
                img_out[i][j] = pix_BGR2HSI(img_in[i][j])
            elif mode == Conv.COLOR_HSI2BGR:
                img_out[i][j] = pix_HSI2BGR(img_in[i][j])
            elif mode == Conv.COLOR_BGR2HSV:
                img_out[i][j] = pix_BGR2HSV(img_in[i][j])
            elif mode == Conv.COLOR_HSV2BGR:
                img_out[i][j] = pix_HSV2BGR(img_in[i][j])
            elif mode == Conv.COLOR_BGR2YUV:
                img_out[i][j] = pix_BGR2YUV(img_in[i][j])
            elif mode == Conv.COLOR_YUV2BGR:
                img_out[i][j] = pix_YUV2BGR(img_in[i][j])
    # 返回转换后的图片
    return img_out
