import matplotlib.pyplot as plt

# 绘制灰度直方图
def pltHist(hist_seq, title, xlabel, ylabel):
    # 设置字体，显示中文
    plt.rcParams["font.family"] = "SimHei"
    plt.rcParams["font.size"] = "12"
    # 设置标题
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    # 绘图
    plt.bar(range(len(hist_seq)), hist_seq)
    plt.show()
