import numpy as np

# 计算灰度直方图序列
def getHist(img, level, chan):
    # 获取图像大小
    shape = img.shape
    height = shape[0]
    width = shape[1]
    # 初始化灰度统计矩阵（全0）
    hist = np.zeros(level, np.int)
    # 统计灰度值
    for i in range(height):
        for j in range(width):
            # img[i][j]为灰度值
            if(chan < 0):
                hist[img[i][j]] += 1
            else:
                hist[img[i][j][chan]] += 1
    # 返回灰度统计数组
    return hist

# 归一化
def getNorm(seq, n):
    # n为seq中所有值的和，由外部传入
    # seq为int数组，需转换为float数组才能存储归一后的值
    size = len(seq)
    n_seq = np.zeros(size, np.float)
    for i in range(size):
        n_seq[i] = seq[i]/n
    return n_seq

# 计算累积分布序列
def getCum(seq):
    # 输入频次序列
    size = len(seq)
    n_seq = np.zeros(size, np.int)
    n_seq[0] = seq[0]
    for i in range(1, size):
        n_seq[i] = seq[i] + n_seq[i-1]
    # 返回累积分布序列（其中存的是频次，不是频率）
    return n_seq

# 计算近似灰度级
def getSim(cum_seq, level):
    # !!! 传入的累积分布序列cum中存的是频率，不是频次
    size = len(cum_seq)
    sim_seq = np.zeros(size, np.int)
    for i in range(size):
        # 取近似灰度级（四舍五入）
        t = cum_seq[i] * (level-1)
        sim_seq[i] = int(t + 0.5)
    # !!! 返回的sim中是近似后的灰度级
    return sim_seq

# 灰度直方图均衡化
def histEq(hist_seq, sim_seq):
    size = len(hist_seq)
    n_hist_seq = np.zeros(size, np.int)
    # 新灰度级相同的旧灰度级合并，对频次求和
    for i in range(0, size):
        n_hist_seq[sim_seq[i]] += hist_seq[i]
    # 返回均衡化后的直方图序列
    return n_hist_seq

# 图像增强（基于直方图均衡）
def imgEnhc(img, level_seq, chan):
    shape = img.shape
    height = shape[0]
    width = shape[1]
    n_img = img.copy()
    for i in range(0, height):
        for j in range(0, width):
            # 灰度图像，新旧灰度值映射
            if(chan < 0):
                n_img[i][j] = level_seq[img[i][j]]
            # 彩色图像，对某个通道映射
            else:
                n_img[i][j][chan] = level_seq[img[i][j][chan]]
    # 返回增强后的图像
    return n_img

