import cv2 as cv
import numpy as np
from math import floor

#  最近邻插值算法
def MyNearest(src_img, target_height, target_width):
    src_height, src_width, src_channels = src_img.shape
    tar_channels = src_channels

    # 高度/宽度上的缩放倍数
    multiple_height = target_height / src_height
    multiple_width = target_width / src_width

    # 生成空的目标图像
    tar_img = np.zeros((target_height, target_width, tar_channels), dtype=np.uint8)
    
    # 渲染像素点的值
    for h in range(target_height):
        for w in range(target_width):
            # 计算新坐标 (h,w) 坐标在旧图中是哪个值
            old_h = round(h/multiple_height)
            old_h = old_h if old_h < src_height else src_height - 1
            old_w = round(w/multiple_width)
            old_w = old_w if old_w < src_width else src_width - 1
            tar_img[h, w] = src_img[old_h, old_w]

    return tar_img
    
#  最近邻插值算法
def MyLinear(src_img, target_height, target_width):
    src_height, src_width, src_channels = src_img.shape
    tar_channels = src_channels

    # 高度/宽度上的缩放倍数
    multiple_height = target_height / src_height
    multiple_width = target_width / src_width

    # 生成空的目标图像
    tar_img = np.zeros((target_height, target_width, tar_channels), dtype=np.uint8)
    
    # 渲染像素点的值
    for h in range(target_height):
        for w in range(target_width):
            # 计算新坐标 (h,w) 坐标在旧图中是哪个值
            i = h/multiple_height
            j = w/multiple_width
            old_h = floor(i)
            old_w = floor(j)
            old_h_add1 = old_h + 1 if old_h < src_height - 1 else src_height - 1
            old_w_add1 = old_w + 1 if old_w < src_width - 1 else src_width - 1
            u = i - old_h
            v = j - old_w
            tar_img[h, w] = (1-u)*(1-v)*src_img[old_h, old_w] + (1-u)*v*src_img[old_h, old_w_add1] + u*(1-v)*src_img[old_h_add1, old_w] + u*v*src_img[old_h_add1, old_w_add1]

    return tar_img


if __name__ == '__main__':
    filename = 'Lena.jpg'
    src = cv.imread(filename)

    dst_cv_Nearest = cv.resize(src, (2880, 2880), interpolation=cv.INTER_NEAREST)
    cv.imwrite('resize_cv2_NEAREST.jpg', dst_cv_Nearest)

    dst_mine_Nearest = MyNearest(src, 2880, 2880)
    cv.imwrite('resize_mine_NEARST.jpg', dst_mine_Nearest)

    dst_cv_Linear = cv.resize(src, (2880, 2880), interpolation=cv.INTER_LINEAR)
    cv.imwrite('resize_cv2_LEARNAR.jpg', dst_cv_Linear)

    dst_mine_Linear = MyLinear(src, 2880, 2880)
    cv.imwrite('resize_mine_LEARNAR.jpg', dst_mine_Linear)