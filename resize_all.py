import cv2 as cv
import numpy as np
import os
from math import floor
import matplotlib.pyplot as plt

#  最近邻插值算法
def MyNearest(src_img, target_height, target_width):
    src_height, src_width, src_channels = src_img.shape # 原图的高度、宽度、通道数
    tar_channels = src_channels

    # 高度/宽度上的缩放倍数
    multiple_height = target_height / src_height
    multiple_width = target_width / src_width

    # 生成空的目标图像
    tar_img = np.zeros((target_height, target_width, tar_channels), dtype=np.uint8)
    
    # 渲染像素点的值
    for h in range(target_height):
        for w in range(target_width):
            # 计算新坐标 (h,w) 坐标在旧图中的坐标
            old_h = round(h/multiple_height)    # 四舍五入
            old_h = old_h if old_h < src_height else src_height - 1     # 避免非法值
            old_w = round(w/multiple_width)     # 四舍五入
            old_w = old_w if old_w < src_width else src_width - 1       # 避免非法值
            # 计算新坐标的像素值
            tar_img[h, w] = src_img[old_h, old_w]

    return tar_img
    
#  双线性插值算法
def MyLinear(src_img, target_height, target_width):
    src_height, src_width, src_channels = src_img.shape # 原图的高度、宽度、通道数
    tar_channels = src_channels

    # 高度/宽度上的缩放倍数
    multiple_height = target_height / src_height
    multiple_width = target_width / src_width

    # 生成空的目标图像
    tar_img = np.zeros((target_height, target_width, tar_channels), dtype=np.uint8)
    
    # 渲染像素点的值
    for h in range(target_height):
        for w in range(target_width):
            # 计算新坐标 (h,w) 坐标在旧图中的坐标
            i = h/multiple_height   # 高度浮点坐标
            j = w/multiple_width    # 宽度浮点坐标
            old_h = floor(i)        # 高度整数坐标
            old_w = floor(j)        # 宽度整数坐标
            old_h_add1 = old_h + 1 if old_h < src_height - 1 else src_height - 1
            old_w_add1 = old_w + 1 if old_w < src_width - 1 else src_width - 1
            u = i - old_h           # 高度 浮动小数值u
            v = j - old_w           # 宽度 浮动小数值v
            # 计算像素值
            tar_img[h, w] = (1-u)*(1-v)*src_img[old_h, old_w] + (1-u)*v*src_img[old_h, old_w_add1] + u*(1-v)*src_img[old_h_add1, old_w] + u*v*src_img[old_h_add1, old_w_add1]

    return tar_img

# def resize_all(rootDir, targetDir, targer_height, target_width):
#     list_dirs = os.walk(rootDir)
#     for path, dirs, files in list_dirs:
#         for f in files:
#             filePath = os.path.join(path, f)
#             src = cv.imread(filePath)
#             dst_cv_Linear = cv.resize(src, (targer_height, target_width), interpolation=cv.INTER_LINEAR)
#             cv.imwrite(targetDir + f, dst_cv_Linear)

def resize_all(rootDir):
    list_dirs = os.walk(rootDir)
    for path, dirs, files in list_dirs:
        for f in files:
            filePath = os.path.join(path, f)
            src = cv.imread(filePath)
            if(src.shape[0] < src.shape[1]):    # 胖图片
                # (w, h) -> (500, 333)  注意与shape相反
                dst_cv_Linear = cv.resize(src, (500, 333), interpolation=cv.INTER_LINEAR)
                cv.imwrite('images_fat_resize/' + f, dst_cv_Linear)
            else:       # 瘦图片
                # (w, h) -> (333, 500)  注意与shape相反
                dst_cv_Linear = cv.resize(src, (333, 500), interpolation=cv.INTER_LINEAR)
                cv.imwrite('images_thin_resize/' + f, dst_cv_Linear)

def checksize_all(rootDir):
    list_dirs = os.walk(rootDir)
    count_height_greater = {}
    count_width_greater = {}
    num_height_greater = 0      # 瘦图片的数量
    num_width_greater = 0       # 胖图片的数量
    sum_height_greater = 0
    sum_width_greater = 0
    for path, dirs, files in list_dirs:
        for f in files:
            filePath = os.path.join(path, f)
            src_img = cv.imread(filePath)
            # print(src_img.shape)
            if(src_img.shape[0] > src_img.shape[1]):
                num_height_greater += 1
                sum_height_greater += src_img.shape[1]
                if(src_img.shape[1] not in count_height_greater):
                    count_height_greater[src_img.shape[1]] = 1
                else:
                    count_height_greater[src_img.shape[1]] += 1
            else:
                num_width_greater += 1
                sum_width_greater += src_img.shape[0]
                if(src_img.shape[0] not in count_width_greater):
                    count_width_greater[src_img.shape[0]] = 1
                else:
                    count_width_greater[src_img.shape[0]] += 1

    ########## 打印信息 ##########
    # print(sum_height_greater/num_height_greater)
    # print(sum_width_greater/num_width_greater)
    # print(count_height_greater)
    # print(count_width_greater)

    # # 设置字体，显示中文
    # plt.rcParams["font.family"] = "SimHei"
    # plt.rcParams["font.size"] = "12"

    # plt.bar(list(count_height_greater.keys()), list(count_height_greater.values()))
    # plt.xlabel('宽度width')
    # plt.ylabel('频数')
    # plt.show()

    # plt.bar(list(count_width_greater.keys()), list(count_width_greater.values()))
    # plt.xlabel('高度height')
    # plt.ylabel('频数')
    # plt.show()

if __name__ == '__main__':
    # resize_all('images_ori', 'images_resize', 256, 256)
    # checksize_all('images_origin')
    resize_all('images_origin')